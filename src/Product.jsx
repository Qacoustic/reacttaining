import React from 'react'

export default function ProductList() {

    const products = [{
        name: 'A'
    }, {
        name: 'B'
    }]

    return <>
        <h1>Product List</h1>
        <hr />
        {
            products.map((item, idx) => <ProductListItem key={idx} name={item.name} />)
        }
    </>
}

function ProductListItem({ name }) {
    return <>
        <ProductName name={name} />
    </>
}

function ProductName({ name }) {
    return <>
        <h1>Product Item: {name}</h1>
    </>
}