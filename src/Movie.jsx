import { useEffect, useState } from 'react';
import { movies } from './db.json'
import _ from 'lodash'

function MovieItem({ movie, handleList }) {
    const [select, setSelect] = useState(false);
    const baseStyle = { cursor: 'pointer' }

    function handleClick() {
        setSelect(!select)
    }

    useEffect(() => {
        handleList(movie.id, select)
    }, [select])

    const style = select ? { ...baseStyle, border: 'solid blue 3px' } : { ...baseStyle };

    return <>
        <div className='column'>
            <img style={style} src={`images/${movie.id}.jpg`} alt={movie.title} onClick={handleClick} />
        </div>
    </>
}

function MovieRow({ row, handleList }) {
    return <>
        <div className='columns'>
            {row.map((item, idx) => <MovieItem key={idx} movie={item} handleList={handleList} />)}
        </div>
    </>
}

export default function Movie() {

    const [list, setList] = useState([])
    const chunk = _.chunk(movies, 3)

    const duration = movies
        .filter(item => list.includes(item.id))
        .reduce((acc, item) => { return acc + item.duration }, 0)

    function handleList(movieId, select) {
        const newList = select ? [...list, movieId] : list.filter(item => item !== movieId)
        setList(newList)
    }

    useEffect(() => {
        console.log(list)
    }, [list])

    return (
        <>
            <pre>{JSON.stringify(list)}</pre>
            <p>Duration: {duration}</p>
            {chunk.map((row, idx) => <MovieRow key={idx} row={row} handleList={handleList} />)}
        </>
    )
}
