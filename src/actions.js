import { FETCH, DONE } from './action-types'
import axios from 'axios'

const api = process.env['REACT_APP_API']

function fetchAction() {
    return {
        type: FETCH
    }
}

function doneAction(payload) {
    return {
        type: DONE,
        payload
    }
}

export function fetchUser() {
    return async function (dispatch) {
        dispatch(fetchAction())

        const { data } = await axios.get(`${api}users`)

        dispatch(doneAction(data))
    }
}