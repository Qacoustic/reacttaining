import { useContext } from 'react'
import { Link } from 'react-router-dom'
import AuthContext from './pages/AuthContext'
import withAuth from './pages/withAuth'

function NavBar({ isAuth, setIsAuth }) {

    // const { isAuth, setIsAuth } = useContext(AuthContext)

    return (
        <>
            <nav className='navbar'>
                <div className='navbar-menu'>
                    <Link to='/' className='navbar-item'>Home</Link>
                    <Link to='/about' className='navbar-item'>About</Link>
                    <Link to='/use-memo' className='navbar-item'>UseMemo</Link>
                    <Link to='/pikaday' className='navbar-item'>Pikaday</Link>
                    <Link to='/use-reducer' className='navbar-item'>UseReducer</Link>
                    <Link to='/context' className='navbar-item'>Context</Link>
                    <Link to='/hoc' className='navbar-item'>HOC</Link>
                    <Link to='/user' className='navbar-item'>User</Link>
                    <Link to='/post' className='navbar-item'>Post</Link>
                    <Link to='/redux' className='navbar-item'>Redux</Link>
                    {
                        isAuth
                            ? <Link to='/context' className='navbar-item' onClick={() => { setIsAuth(!isAuth) }}>Logout</Link>
                            : <Link to='/context' className='navbar-item' onClick={() => { setIsAuth(!isAuth) }}>Login</Link>
                    }
                </div>
            </nav>

        </>
    )
}


export default withAuth(NavBar)