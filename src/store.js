import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { FETCH, DONE } from './action-types'

const initialState = {
    loading: false,
    users: [],
    pages: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH:
            return {
                ...state,
                loading: true,
                user: [],
                pages: []
            }
        case DONE:
            return {
                ...state,
                loading: false,
                users: action.payload,
                pages: []
            }
    }
    return state
}

const reducer2 = (state = { foo: 'FOO' }) => {
    return state
}

const reducers = combineReducers({
    a: reducer,
    b: reducer2
})
export default createStore(reducers, applyMiddleware(thunk))
// export default createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk))