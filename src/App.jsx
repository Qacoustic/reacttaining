import 'bulma/css/bulma.min.css'
import NavBar from './NavBar'
import Routes from './Routes'
import AuthContext from './pages/AuthContext'
import { useState } from 'react'

export default function App() {

    const [isAuth, setIsAuth] = useState(false)

    return (
        <>
            <section className='section'>
                <div className='container'>
                    <AuthContext.Provider value={{ isAuth, setIsAuth }}>
                        <NavBar />
                        <Routes />
                    </AuthContext.Provider>
                </div>
            </section>
        </>
    )
}
