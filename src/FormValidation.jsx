import React, { useEffect, useState } from 'react'
import Validator from 'validatorjs'

export default function FormValidation() {

    const [person, setPerson] = useState({
        name: '',
        surname: ''
    })

    const [errors, setError] = useState([])

    function handleChange(e) {
        setPerson({
            ...person,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        const rules = {
            name: 'required|min:3',
            surname: 'required|min:3'
        }

        const validator = new Validator(person, rules)

        if (validator.fails()) {
            console.log(validator.errors.first('name'))
            console.log(validator.errors.errors)
        }

        const newErrors = Object.keys(rules).reduce((acc, item) => {
            return {
                ...acc,
                [item]: validator.errors.first(item)
            }
        }, {})

        setError(newErrors);

    }, [person])

    return (
        <>
            {JSON.stringify(errors)}
            <div className='field'>
                <input value={person.name} name='name' onChange={handleChange} type='text' className={`input ${errors['name'] ? 'is-danger' : ''} `} />
                <p className="help is-danger">{errors['name'] && errors['name']}</p>
            </div>
            <div className='field'>
                <input value={person.surname} name='surname' onChange={handleChange} type='text' className={`input ${errors['surname'] ? 'is-danger' : ''}`} />
                <p className="help is-danger">{errors['surname'] && errors['surname']}</p>
            </div>
        </>
    )
}


// function useFormInput(initialValue) {
//     const [value, setValue] = useState(initialValue)

//     function handleChange(e) {
//         setValue(e.target.value)
//     }

//     return [value, handleChange]
// }


// export default function HelloWorld() {

//     // const [person, setPerson] = useState({
//     //     name: '',
//     //     surname: ''
//     // })

//     // function handleChange(e) {
//     //     setPerson({
//     //         ...person,
//     //         [e.target.name]: e.target.value
//     //     })
//     // }

//     const [name, handleNameChange] = useFormInput('');
//     const [surname, handleSurnameChange] = useFormInput('');

//     return (
//         <>
//             <input value={name} name='name' onChange={handleNameChange} type="text" />
//             <input value={surname} name='surname' onChange={handleSurnameChange} type="text" />
//             <pre>{JSON.stringify({ name, surname })}</pre>
//         </>
//     )
// }
