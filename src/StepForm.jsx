import React, { useState } from 'react'
// import './style.css'
//import style from './HelloWorld.module.css'

export default function HelloWorld() {

    const [step, setStep] = useState(1)

    function Step1() {
        return <h1>Step1</h1>
    }

    function Step2() {
        return <h1>Step2</h1>
    }

    function Step3() {
        return <h1>Step3</h1>
    }

    function handleClick(n) {
        setStep(step + n);
    }

    return <>
        {step === 1 && <Step1 />}
        {step === 2 && <Step2 />}
        {step === 3 && <Step3 />}
        {step != 1 && <button onClick={() => handleClick(-1)}>{'<-- Previous'}</button>}
        {step != 3 && <button onClick={() => handleClick(1)}>{'Next -->'}</button>}
    </>
}




// const style = {
//     color: 'red'
// }

// console.log(style)

// let counter = 1

// setTimeout(() => {
//     counter = 100
//     console.log(counter)
// }, 1000)

// export default function HelloWorld() {

//     // const red = true;
//     // const numbers = [1, 2, 3, 4, 5];

//     const [counter, setCounter] = useState(0)
//     const [name, setName] = useState("")

//     function handleClick() {
//         setCounter(counter + 1)
//     }

//     function handleClickName() {
//         setName("Ton")
//     }

//     const title = (counter % 2) ? 'odd' : 'even';

//     return (
//         <>
//             {counter} : {title}
//             <br />
//             <button onClick={handleClick}>Update</button>

//             <p>name: {name}</p>
//             <button onClick={handleClickName}>Update Name</button>
//             <h1 className={style.bg}>Hello</h1>
//             {/*<h1 className={style.red, style.size}>Hello</h1>
//             <h1 className={style.xxx}>World</h1>
//             {
//                 red
//                     ? <h1 className={style.red}>Hello</h1>
//                     : <h1 >Hello</h1>
//             }
//             {
//                 numbers.map((num, idx) => <p key={idx}>{num}</p>)
//             }
//             {
//                 numbers.map((num, idx) => (num % 2) ? <p key={idx}>Odd {num}</p> : <p>Even {num}</p>)
//             } */}


//         </>
//     )
// }
