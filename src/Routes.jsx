import { Switch, Route } from 'react-router-dom'
import UseMemo from './pages/UseMemo'
import Pikaday from './pages/Pikaday'
import UseReducer from './pages/UseReducer'
import Context from './pages/Context'
import HOC from './pages/HOC'
import User from './pages/User'
import Post from './pages/Post'
import Redux from './pages/Redux'

function Home() {
    return <h1 className='title'>Home</h1>
}

function About() {
    return <h1 className='title'>About</h1>
}

export default function Routes() {
    return (
        <>
            <Switch>
                <Route exact={true} path='/' component={Home} />
                <Route path='/about' component={About} />
                <Route path='/use-memo' component={UseMemo} />
                <Route path='/pikaday' component={Pikaday} />
                <Route path='/use-reducer' component={UseReducer} />
                <Route path='/context' component={Context} />
                <Route path='/hoc' component={HOC} />
                <Route path='/user' component={User} />
                <Route path='/post' component={Post} />
                <Route path='/redux' component={Redux} />
            </Switch>
        </>
    )
}
