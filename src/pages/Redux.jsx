import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
// import { fetchAction, doneAction } from '../actions'
import { fetchUser } from '../actions'

export default function Redux() {

    const state = useSelector(mapStateToProps)
    const dispatch = useDispatch()



    function handleClick() {
        dispatch(fetchUser())
    }

    return (
        <div>
            <button className="button" onClick={handleClick}>Fetch</button>
            { state.loading && <Loading />}
            <pre>{JSON.stringify(state)}</pre>
        </div>
    )
}


// export default connect(mapStateToProps)(function Redux({ dispatch, loading, users }) {

//     console.log(loading, users, dispatch)

//     return <>
//         <h1 className="title">Redux</h1>
//         {/* <pre>{JSON.stringify(props)}</pre> */}
//     </>
// })

function mapStateToProps({ a: { loading, users } }) {
    return { loading, users }
}

function Loading() {
    return <>
        <h1 className="title">...Loading</h1>
    </>
}
