import { useState, useMemo, useEffect } from "react"
import axios from 'axios'

function Paginator({ pages = [], currentPage = 0, setCurrentPage }) {
    return <nav className="pagination" role="navigation" aria-label="pagination">
        <ul className="pagination-list">
            {
                pages.map((item, idx) => (
                    <li key={idx}>
                        <a className={`pagination-link ${currentPage === item && 'is-current'}`} onClick={() => { setCurrentPage(item) }}>{item}</a>
                    </li>))
            }
        </ul>
    </nav>
}

function PaginationFetch({ uri, currentPage, children }) {
    const [posts, setPosts] = useState([])
    const [pages, setPages] = useState([])

    async function getPost() {

        if (currentPage === 0) {
            return
        }
        const { data, headers } = await axios.get(`${uri}?_limit=9&_page=${currentPage}`)
        setPosts(data)

        if (currentPage === 1) {
            const totalPage = Math.floor(+headers['x-total-count'] / 9)

            setPages(new Array(totalPage).fill().map((item, idx) => idx + 1))
        }
    }

    useEffect(() => {
        getPost();
    }, [currentPage])

    return <>
        {
            children({ posts, pages })
        }
    </>
}

export default function Post() {
    const api = useMemo(() => `${process.env['REACT_APP_API']}posts`, [])
    const [currentPage, setCurrentPage] = useState(0)

    return <>
        <button className="button" onClick={() => setCurrentPage(1)}>Fetch</button>
        <PaginationFetch uri={api} currentPage={currentPage}>
            {
                ({ posts, pages }) => {
                    return <>
                        <pre>{JSON.stringify(posts)}</pre>
                        <Paginator pages={pages} currentPage={currentPage} setCurrentPage={setCurrentPage} />
                    </>
                }
            }
        </PaginationFetch>
    </>
}

// export default function Post() {
//     const url = useMemo(() => process.env['REACT_APP_API'], [])
//     const [posts, setPosts] = useState([])
//     const [pages, setPages] = useState([])
//     const [currentPage, setCurrentPage] = useState(0)

//     async function getPost() {

//         if (currentPage === 0) {
//             return
//         }
//         const { data, headers } = await axios.get(`${url}posts?_limit=9&_page=${currentPage}`)
//         setPosts(data)

//         if (currentPage === 1) {
//             const totalPage = Math.floor(+headers['x-total-count'] / 9)

//             setPages(new Array(totalPage).fill().map((item, idx) => idx + 1))
//         }
//     }

//     useEffect(() => {
//         getPost();
//     }, [currentPage])

//     return <>
//         <h1 className='title'>Post</h1>
//         <button className="button" onClick={() => setCurrentPage(1)}>Fetch</button>
//         <hr />
//         <pre>{JSON.stringify(posts)}</pre>
//         <pre>{JSON.stringify(pages)}</pre>
//         <Paginator pages={pages} currentPage={currentPage} setCurrentPage={setCurrentPage} />

//     </>
// }
