import { useState } from 'react'

function withCounter(WrappedComponent) {
    return function (props) {

        const [counter, setCounter] = useState(1)

        return <WrappedComponent {...props} counter={counter} setCounter={setCounter} />
    }
}

function Dumb(props) {
    return <>
        {
            props.setCounter
            && <button className="button" onClick={() => props.setCounter(props.counter + 1)}>Update {props.counter}</button>
        }
    </>
}

const DumbWithCounter = withCounter(Dumb)

export default function HOC() {

    return (
        <>
            <h1 className="title">HOC</h1>
            <hr />
            <Dumb hello="world" />
            <DumbWithCounter hello="world" />
        </>
    )
}
