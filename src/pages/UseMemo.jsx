import { useMemo, useState } from 'react'
export default function UseMemo() {

    const [counter, setCounter] = useState(1)

    const a = Math.random()
    const b = useMemo(() => Math.random(), [])

    console.log(b)

    return (
        <>
            <h1 className='title'>UseMemo</h1>
            <button onClick={() => setCounter(counter + 1)} className='button'>Update {counter}</button>
        </>
    )
}
