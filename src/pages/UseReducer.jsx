import React, { useReducer } from 'react'

const initialState = {
    items: [],
    loading: false
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'FETCH':
            return { ...state, loading: true, item: [] }
        case 'DONE':
            return { ...state, loading: false, item: [1, 2, 3, 4] }
    }
    return state
}

export default function UseReducer() {

    const [state, dispatch] = useReducer(reducer, initialState)

    function handleClick() {
        dispatch({ type: 'FETCH' })
        setTimeout(() => {
            dispatch({ type: 'DONE' })
        }, 3000)
    }

    return (
        <>
            <pre>{JSON.stringify(state)}</pre>
            <h1 className="title">Use Reducer</h1>
            <button onClick={handleClick} className='button'>Fetch</button>
        </>
    )
}
