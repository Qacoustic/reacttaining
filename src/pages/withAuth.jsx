import { useContext } from 'react'
import AuthContext from './AuthContext'

export default function withAuth(WrappedComponent) {

    return function (props) {

        const { isAuth, setIsAuth } = useContext(AuthContext)

        return <>
            <WrappedComponent {...props} isAuth={isAuth} setIsAuth={setIsAuth} />
        </>
    }
}