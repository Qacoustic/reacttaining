import { createContext, useContext, useState } from 'react'
import AuthContext from './AuthContext'

const CounterContext = createContext({ counter: -1 })

function Container({ children }) {
    return <>
        {children}
    </>
}

function Child() {

    const { counter, setCounter } = useContext(CounterContext)
    const { isAuth, setIsAuth } = useContext(AuthContext)

    debugger;

    return <>
        <h1 className="title">Child: {counter}</h1>
        <p>
            <button className="button" onClick={() => setCounter(counter + 1)}>Update</button>
        </p>
        <p>
            {
                isAuth
                    ? <button className="button is-danger" onClick={() => { setIsAuth(!isAuth) }}>Logout</button>
                    : <button className="button is-primary" onClick={() => { setIsAuth(!isAuth) }}>Login</button>
            }
        </p>
    </>
}

export default function Context() {

    const [counter, setCounter] = useState(1)

    return (
        <>
            <h1 className="title">Context</h1>
            <CounterContext.Provider value={{ counter, setCounter }}>
                <Child />
                <Container>
                    <Child />
                </Container>
            </CounterContext.Provider>
        </>
    )
}
