import React, { useState, useMemo, useEffect } from 'react'
import axios from 'axios'

export default function User() {

    const [users, setUsers] = useState([])
    const [userDetail, setUserDetail] = useState({})
    const [userId, setUserId] = useState(-1)
    const url = useMemo(() => process.env['REACT_APP_API'], [])

    async function handleClick() {
        try {
            const { data: users } = await axios.get(`${url}users`)
            setUsers(users);
        }
        catch (e) {
            console.log(e.message)
        }
    }

    async function handleNameClick(id) {
        setUserId(id)
    }

    async function getUserDetail(source) {
        try {
            const { data: user } = await axios.get(`${url}users/${userId}`, { cancelToken: source.token })
            setUserDetail(user);
        }
        catch (e) {
            console.log(e.message)
        }
    }

    useEffect(() => {

        if (userId === -1) {
            return
        }

        const source = axios.CancelToken.source()

        getUserDetail(source);

        return () => {
            source.cancel('abort')
        }
    }, [userId])


    return (
        <>
            <h1 className='title'>User</h1>
            <hr />
            {
                users.map((item, idx) => <a style={{ display: 'block' }} key={idx} onClick={() => handleNameClick(item.id)}>{item.name}</a>)
            }
            <button className="button" onClick={handleClick}>Fetch</button>
            <pre>{JSON.stringify(userDetail)}</pre>
        </>
    )
}
