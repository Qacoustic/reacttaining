import 'pikaday/css/pikaday.css'
import Pikaday from 'pikaday'
import { createRef, useEffect } from 'react'

export default function () {

    const el = createRef()

    useEffect(() => {
        const pikaday = new Pikaday({
            field: el.current
        })

        return () => {
            pikaday.destroy()
        }
    }, [])

    return (
        <>
            <h1 className='title'>Pick a day</h1>
            <input type='text' className='input' ref={el} name="444" />

        </>
    )
}

