import React, { useEffect, useState } from 'react'

function Clock() {
    const [now, setNow] = useState(new Date())

    useEffect(() => {
        const ref = setInterval(() => {
            console.log('1')
            setNow(new Date())
        }, 1000)

        return () => {
            console.log('3')
            clearInterval(ref)
        }
    }, [])

    return <p>{now.toTimeString()}</p>
}

export default function HelloWorld() {

    const [show, setShow] = useState(false)

    return <>
        <p>
            <button onClick={() => setShow(!show)}>Toggle</button>
        </p>
        { show && <Clock />}
    </>
    // const [counter, setCounter] = useState(1)
    // const [n, setN] = useState(1)

    // useEffect(() => {
    //     console.log('1')
    //     // return () => {
    //     //     cleanup
    //     // }
    // }, [])

    // useEffect(() => {
    //     console.log('2')
    // }, [counter])

    // return <div>
    //     <button onClick={() => setCounter(counter + 1)}>{counter}</button>
    // </div>

}
