import React from 'react'

function Container(props) {
    return <>
        <h1>Header</h1>
        {props.children}
    </>
}

function Hello(props) {
    return <>
        {props.children('world')}
    </>
}

export default function () {
    return <>
        {/* <Container>
            <h1>Sub Header</h1>
            <p>a;kskdj;la k;ldk a;lsdj;alsdlj hasl;dkla jsjdl; lajs's,mkdlkjasl;dklkajdl;dkas;ldkas;lkd </p>
        </Container> */}
        <Hello>
            {(context) => {
                return <h3>{context}</h3>
            }}
        </Hello>
    </>
}

