import React, { useState } from 'react'

function Navigator({ step, handleClick }) {
    return <>
        {step != 1 && <button onClick={() => handleClick(-1)}>{'<-- Previous'}</button>}
        {step != 3 && <button onClick={() => handleClick(1)}>{'Next -->'}</button>}
    </>
}

export default function HelloWorld() {

    const [step, setStep] = useState(1)

    function Step1() {
        return <h1>Step1</h1>
    }

    function Step2() {
        return <h1>Step2</h1>
    }

    function Step3() {
        return <h1>Step3</h1>
    }

    function handleClick(n) {
        setStep(step + n);
    }

    return <>
        {step === 1 && <Step1 />}
        {step === 2 && <Step2 />}
        {step === 3 && <Step3 />}
        <Navigator step={step} handleClick={handleClick} />
    </>
}
